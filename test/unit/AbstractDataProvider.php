<?php

namespace Test\Unit;

use Samy\Dummy\Random;

abstract class AbstractDataProvider
{
    protected function csv(string $Basename): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . $Basename . ".csv";

        if (is_file($filename)) {
            $file = fopen($filename, "r");

            if ($file) {
                while (($data = fgetcsv($file)) !== false) {
                    array_push($ret, $data);
                }

                fclose($file);
            }
        }

        return $ret;
    }


    // $Code
    public function dataSourceTarget(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->string()));
        }

        return $ret;
    }

    // $TextActual, $TextExpect
    public function dataText(): array
    {
        $ret = array();

        foreach ($this->csv("text") as $data) {
            $count = count($data);

            array_push($ret, array(
                ($count > 0 ? $data[0] : 0),
                ($count > 1 ? $data[1] : "")
            ));
        }

        return $ret;
    }


    // $Text
    public function dataTranslateEn(): array
    {
        $ret = array();

        foreach ($this->csv("translate-en") as $data) {
            $count = count($data);

            array_push($ret, array(($count > 0 ? $data[0] : "")));
        }

        return $ret;
    }
}
