<?php

namespace Test\Unit;

use Samy\Translate\MyMemory;

class MyMemoryTest extends AbstractTranslate
{
    protected function setUp(): void
    {
        $this->translate = new MyMemory();
    }


    /**
     * @dataProvider \Test\Unit\MyMemoryDataProvider::dataSupportedLanguages
     */
    public function testMyMemorySupportedLanguages($SupportedLanguages): void
    {
        $this->assertSame($SupportedLanguages, $this->translate->getSupportedLanguages());
    }


    /**
     * @dataProvider \Test\Unit\MyMemoryDataProvider::dataEmail
     */
    public function testMyMemoryEmail($Email): void
    {
        $this->assertInstanceOf(MyMemory::class, $this->translate->withEmail($Email));
        $this->assertSame($Email, $this->translate->getEmail());
    }


    /**
     * @dataProvider \Test\Unit\MyMemoryDataProvider::dataSourceTarget
     */
    public function testMyMemorySourceTarget($Code): void
    {
        $this->assertSource($Code);
        $this->assertTarget($Code);
    }

    /**
     * @dataProvider \Test\Unit\MyMemoryDataProvider::dataText
     */
    public function testMyMemoryText($TextActual, $TextExpect): void
    {
        $this->assertText($TextActual, $TextExpect);
    }


    /**
     * @dataProvider \Test\Unit\MyMemoryDataProvider::dataTranslateEn
     */
    public function testMyMemoryTranslate($Text): void
    {
        $this->assertInstanceOf(
            MyMemory::class,
            $this->translate
                ->withSource("en")
                ->withTarget("id")
                ->withText($Text)
        );

        $this->assertNotSame($Text, $this->translate->translate());
    }
}
