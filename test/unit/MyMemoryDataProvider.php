<?php

namespace Test\Unit;

use Samy\Dummy\Random;

class MyMemoryDataProvider extends AbstractDataProvider
{
    // $SupportedLanguages
    public function dataSupportedLanguages(): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . "rfc-3066.json";
        $content = @file_get_contents($filename);

        if (is_string($content)) {
            $json = json_decode($content, true);

            if (is_array($json)) {
                array_push($ret, array($json));
            }
        }

        return $ret;
    }


    // $Email
    public function dataEmail(): array
    {
        $ret = array();
        $random = new Random();

        $count = $random->integer(5, 10);
        for ($index = 0; $index < $count; $index++) {
            array_push($ret, array($random->email()));
        }

        return $ret;
    }
}
