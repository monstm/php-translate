<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use Samy\Translate\AbstractTranslate as TranslateAbstractTranslate;

class AbstractTranslate extends TestCase
{
    protected $translate;


    protected function assertSource($Source): void
    {
        $this->assertInstanceOf(TranslateAbstractTranslate::class, $this->translate->withSource($Source));
        $this->assertSame($Source, $this->translate->getSource());
    }

    protected function assertTarget($Target): void
    {
        $this->assertInstanceOf(TranslateAbstractTranslate::class, $this->translate->withTarget($Target));
        $this->assertSame($Target, $this->translate->getTarget());
    }

    protected function assertText($TextActual, $TextExpect): void
    {
        $this->assertInstanceOf(TranslateAbstractTranslate::class, $this->translate->withText($TextActual));
        $this->assertSame($TextExpect, $this->translate->getText());
    }
}
