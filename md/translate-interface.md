# Translate Interface

Describes Translate interface.

---

## getSupportedLanguages

Retrieve supported languages.

```php
$languages = $translate->getSupportedLanguages();
```

---

## withSource

Return an instance with provided source language.

```php
$translate = $translate->withSource($source);
```

---

## getSource

Retrieve provided source language.

```php
$source = $translate->getSource();
```

---

## withTarget

Return an instance with provided target language.

```php
$translate = $translate->withTarget($target);
```

---

## getTarget

Retrieve provided target language.

```php
$target = $translate->getTarget();
```

---

## withText

Return an instance with provided text.

```php
$translate = $translate->withText($text);
```

---

## getText

Retrieve provided text.

```php
$text = $translate->getText();
```

---

## translate

Retrieve translated text.

```php
$translated_text = $translate->translate();
```
