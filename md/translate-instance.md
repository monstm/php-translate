# Translate Instance

Simple Translate Implementation.

---

## MyMemory

Implementation MyMemory instance.

```php
$translate = MyMemory();
```

### withEmail

Return an instance with provided email.

```php
$paraphrase = $paraphrase->withEmail($email);
```

### getEmail

Retrieve provided email.

```php
$email = $paraphrase->getEmail();
```

### getUsageCharacters

Retrieve usage characters.

```php
$usage_characters = $paraphrase->getUsageCharacters();
```


