<?php
	/*
	 * @see http://www.i18nguy.com/unicode/language-identifiers.html
	 */

	define("LIST_FILE", __DIR__ . DIRECTORY_SEPARATOR . "rfc-3066.lst");
	define("JSON_TRANSLATE", dirname(__DIR__) . DIRECTORY_SEPARATOR . "translate" . DIRECTORY_SEPARATOR . "rfc-3066.json");
	define("JSON_TEST", dirname(__DIR__) . DIRECTORY_SEPARATOR . "test" . DIRECTORY_SEPARATOR . "unit" . DIRECTORY_SEPARATOR . "rfc-3066.json");


	function main(): int{
		$result = array();
		$content = @file_get_contents(LIST_FILE);

		if(is_string($content)){
			$unique = array();

			foreach(explode("\n", $content) as $line){
				$data = explode("\t", $line, 2);
				$count = count($data);

				$code = ($count > 0 ? strtolower(trim($data[0])) : "");
				$name = ($count > 1 ? trim($data[1]) : "");

				if(($code != "") && ($name != "") && !in_array($code, $unique)){
					array_push($result, array("code" => $code, "name" => $name));
					array_push($unique, $code);
				}
			}
		}

		$json_encode = json_encode($result);

		@file_put_contents(JSON_TRANSLATE, $json_encode);
		@file_put_contents(JSON_TEST, $json_encode);

		return 0;
	}


	exit(main());
?>