# PHP Translate

[
	![](https://badgen.net/packagist/v/samy/translate/latest)
	![](https://badgen.net/packagist/license/samy/translate)
	![](https://badgen.net/packagist/dt/samy/translate)
	![](https://badgen.net/packagist/favers/samy/translate)
](https://packagist.org/packages/samy/translate)

Try the Translate API for a simple and affordable programmatic interface using Neural Machine Translation to translate web content.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/translate
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-translate>
* Documentations: <https://monstm.gitlab.io/php-translate/>
* Annotation: <https://monstm.alwaysdata.net/php-translate/>
* Issues: <https://gitlab.com/monstm/php-translate/-/issues>
