<?php

namespace Samy\Translate;

use Psr\Http\Message\ResponseInterface;
use Samy\Psr18\Client;
use Samy\Psr7\Request;
use Samy\Psr7\Stream;
use Samy\Psr7\Uri;

/**
 * This is a simple Translate implementation that other Translate can inherit from.
 */
abstract class AbstractTranslate implements TranslateInterface
{
    /** describe source language */
    protected $source = "";

    /** describe target language */
    protected $target = "";

    /** describe the original text */
    protected $text = "";


    /**
     * Retrieve supported languages.
     *
     * @return array<array<string, string>>
     */
    abstract public function getSupportedLanguages(): array;

    /**
     * Retrieve translated text.
     *
     * @return string
     */
    abstract public function translate(): string;


    /**
     * Return an instance with provided source language.
     *
     * @param[in] string $Source The source code
     *
     * @return static
     */
    public function withSource(string $Source): self
    {
        $this->source = $Source;

        return $this;
    }

    /**
     * Retrieve provided source language.
     *
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * Return an instance with provided target language.
     *
     * @param[in] string $Target The target code
     *
     * @return static
     */
    public function withTarget(string $Target): self
    {
        $this->target = $Target;

        return $this;
    }

    /**
     * Retrieve provided target language.
     *
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }


    /**
     * Return an instance with provided text.
     *
     * @param[in] string $Text the original text
     *
     * @return static
     */
    public function withText(string $Text): self
    {
        $this->text = preg_replace("/\s+/", " ", trim($Text));

        return $this;
    }

    /**
     * Retrieve provided text.
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }


    /**
     * Retrieve splited text characters.
     *
     * @param[in] int $Limit Characters limit
     *
     * @return array<string>
     */
    protected function splitTextCharacter(int $Limit): array
    {
        $ret = array();

        $buffer = "";
        foreach ($this->getSentences() as $sentence) {
            if (($buffer != "") && ((strlen($buffer) + strlen($sentence)) > $Limit)) {
                array_push($ret, $buffer);
                $buffer = "";
            }

            $buffer .= ($buffer == "" ? $sentence : " " . $sentence);
        }

        if ($buffer != "") {
            array_push($ret, $buffer);
        }

        return $ret;
    }

    /**
     * Retrieve text sentences.
     *
     * @param[in] int $Limit Characters limit
     *
     * @return array<string>
     */
    protected function getSentences(): array
    {
        $ret = array();

        $sentences = explode(". ", $this->text);
        $count = count($sentences);
        $limit = $count - 1;

        for ($index = 0; $index < $limit; $index++) {
            $trim = trim($sentences[$index]);
            if ($trim != "") {
                array_push($ret, $trim . ".");
            }
        }

        $trim = trim($sentences[$limit]);
        if ($trim != "") {
            array_push($ret, $trim);
        }

        return $ret;
    }


    /**
     * Send PSR-7 request.
     *
     * @param[in] string $Method Request method
     * @param[in] string $EndPoint Request end point
     * @param[in] array $Headers Request headers
     * @param[in] string $Content Request Body
     *
     * @return ResponseInterface
     */
    protected function request(string $Method, string $EndPoint, array $Headers, string $Content): ResponseInterface
    {
        $request = new Request();

        foreach ($Headers as $name => $value) {
            $request->withHeader($name, $value);
        }

        $stream = new Stream();
        $stream->withTemp();
        $stream->write($Content);
        $stream->rewind();

        $uri = new Uri();
        $uri->parseUrl($EndPoint);

        $request
            ->withMethod($Method)
            ->withBody($stream)
            ->withUri($uri);

        $client = new Client();

        return $client->sendRequest($request);
    }
}
