<?php

namespace Samy\Translate;

/**
 * Describes Translate interface.
 */
interface TranslateInterface
{
    /**
     * Retrieve supported languages.
     *
     * @return array<array<string, string>>
     */
    public function getSupportedLanguages(): array;


    /**
     * Return an instance with provided source language.
     *
     * @param[in] string $Source The source code
     *
     * @return static
     */
    public function withSource(string $Source): self;

    /**
     * Retrieve provided source language.
     *
     * @return string
     */
    public function getSource(): string;


    /**
     * Return an instance with provided target language.
     *
     * @param[in] string $Target The target code
     *
     * @return static
     */
    public function withTarget(string $Target): self;

    /**
     * Retrieve provided target language.
     *
     * @return string
     */
    public function getTarget(): string;


    /**
     * Return an instance with provided text.
     *
     * @param[in] string $Text the original text
     *
     * @return static
     */
    public function withText(string $Text): self;

    /**
     * Retrieve provided text.
     *
     * @return string
     */
    public function getText(): string;


    /**
     * Retrieve translated text.
     *
     * @return string
     */
    public function translate(): string;
}
