<?php

namespace Samy\Translate;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Samy\Log\Syslog;

/**
 * Simple Tinq AI implementation.
 */
class MyMemory extends AbstractTranslate
{
    /** describe characters request limit */
    protected $characters_request = 500;

    /** describe delegate email */
    protected $email = "";

    /** describe usage characters */
    protected $usage_characters = 0;


    /**
     * Return an instance with provided email.
     *
     * @param[in] string $Email The delegate email
     *
     * @return static
     */
    public function withEmail(string $Email): self
    {
        if (($this->email != $Email) && filter_var($Email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $Email;
            $this->usage_characters = 0;
        }

        return $this;
    }

    /**
     * Retrieve provided email.
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }


    /**
     * Retrieve usage characters.
     *
     * @return int
     */
    public function getUsageCharacters(): int
    {
        return $this->usage_characters;
    }


    /**
     * Retrieve supported languages.
     *
     * @return array<array<string, string>>
     */
    public function getSupportedLanguages(): array
    {
        $ret = array();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . "rfc-3066.json";
        $content = @file_get_contents($filename);

        if (is_string($content)) {
            $json = json_decode($content, true);

            if (is_array($json)) {
                $ret = $json;
            }
        }

        return $ret;
    }


    /**
     * Retrieve translated text.
     *
     * @return string
     */
    public function translate(): string
    {
        if ($this->source != $this->target) {
            $buffer = array();

            foreach ($this->splitTextCharacter($this->characters_request) as $text) {
                $this->usage_characters += strlen($text);

                $response = $this->request(
                    "GET",
                    "https://api.mymemory.translated.net/get" .
                        "?q=" . urlencode($text) .
                        "&langpair=" . urlencode($this->source) . "|" . urlencode($this->target) .
                        ($this->email != "" ? "&de=" . urlencode($this->email) : ""),
                    array(),
                    ""
                );

                $result = $this->parseResponse($response);
                if ($result != "") {
                    array_push($buffer, $result);
                }
            }

            $ret = implode(" ", $buffer);
        } else {
            $ret = $this->text;
        }

        return $ret;
    }

    /**
     * parse PSR-7 response interface.
     *
     * @return string
     */
    private function parseResponse(ResponseInterface $ResponseInterface): string
    {
        $ret = "";
        $log = new Syslog();

        try {
            $body = $ResponseInterface->getBody();

            $body->rewind();
            $content = $body->getContents();
            $body->rewind();

            if ($ResponseInterface->getStatusCode() == 200) {
                $json = @json_decode($content, true);

                if (is_array($json)) {
                    $status = ($json["responseStatus"] ?? 500);

                    if ($status == 200) {
                        $ret = ($json["translate"] ?? "");
                        $data = ($json["responseData"] ?? array());

                        if (isset($data["translatedText"]) && is_string($data["translatedText"])) {
                            $ret = $data["translatedText"];
                        }
                    } else {
                        $details = ($json["responseDetails"] ?? "");
                        $log->backtrace($details);
                    }
                } else {
                    $log->backtrace(json_last_error_msg());
                }
            } else {
                $log->backtrace($content);
            }
        } catch (Exception $exception) {
            $log = $log->exception($exception);
        }

        return $ret;
    }
}
